﻿namespace RacunarskaGrafika.Vezbe.AssimpNetSample
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            this.m_world.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openglControl = new Tao.Platform.Windows.SimpleOpenGlControl();
            this.panelParams = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.lblAnimate = new System.Windows.Forms.Label();
            this.numScale = new System.Windows.Forms.NumericUpDown();
            this.lblScale = new System.Windows.Forms.Label();
            this.numRotate = new System.Windows.Forms.NumericUpDown();
            this.lblRotate = new System.Windows.Forms.Label();
            this.numTranslate = new System.Windows.Forms.NumericUpDown();
            this.lblTranslate = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.gbControls = new System.Windows.Forms.GroupBox();
            this.lblTut1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panelParams.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRotate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTranslate)).BeginInit();
            this.gbControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // openglControl
            // 
            this.openglControl.AccumBits = ((byte)(0));
            this.openglControl.AutoCheckErrors = false;
            this.openglControl.AutoFinish = false;
            this.openglControl.AutoMakeCurrent = true;
            this.openglControl.AutoSwapBuffers = true;
            this.openglControl.BackColor = System.Drawing.Color.Black;
            this.openglControl.ColorBits = ((byte)(32));
            this.openglControl.DepthBits = ((byte)(16));
            this.openglControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.openglControl.Location = new System.Drawing.Point(170, 0);
            this.openglControl.Margin = new System.Windows.Forms.Padding(4);
            this.openglControl.Name = "openglControl";
            this.openglControl.Size = new System.Drawing.Size(1093, 692);
            this.openglControl.StencilBits = ((byte)(0));
            this.openglControl.TabIndex = 1;
            this.openglControl.Paint += new System.Windows.Forms.PaintEventHandler(this.OpenGlControlPaint);
            this.openglControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OpenGlControlKeyDown);
            this.openglControl.Resize += new System.EventHandler(this.OpenGlControlResize);
            // 
            // panelParams
            // 
            this.panelParams.Controls.Add(this.gbControls);
            this.panelParams.Controls.Add(this.btnReset);
            this.panelParams.Controls.Add(this.lblAnimate);
            this.panelParams.Controls.Add(this.numScale);
            this.panelParams.Controls.Add(this.lblScale);
            this.panelParams.Controls.Add(this.numRotate);
            this.panelParams.Controls.Add(this.lblRotate);
            this.panelParams.Controls.Add(this.numTranslate);
            this.panelParams.Controls.Add(this.lblTranslate);
            this.panelParams.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelParams.Location = new System.Drawing.Point(0, 0);
            this.panelParams.Name = "panelParams";
            this.panelParams.Size = new System.Drawing.Size(163, 692);
            this.panelParams.TabIndex = 2;
            // 
            // btnReset
            // 
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Location = new System.Drawing.Point(23, 257);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(120, 47);
            this.btnReset.TabIndex = 7;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lblAnimate
            // 
            this.lblAnimate.AutoSize = true;
            this.lblAnimate.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblAnimate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnimate.Location = new System.Drawing.Point(0, 0);
            this.lblAnimate.Name = "lblAnimate";
            this.lblAnimate.Size = new System.Drawing.Size(171, 232);
            this.lblAnimate.TabIndex = 6;
            this.lblAnimate.Text = "Animacija\r\n\"Trka F1\"\r\n\r\nFerrari\r\n(S. Vettel)\r\nvs\r\nMercedes\r\n(L. Hamilton)";
            this.lblAnimate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblAnimate.Visible = false;
            // 
            // numScale
            // 
            this.numScale.DecimalPlaces = 2;
            this.numScale.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numScale.Location = new System.Drawing.Point(8, 192);
            this.numScale.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numScale.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numScale.Name = "numScale";
            this.numScale.Size = new System.Drawing.Size(120, 22);
            this.numScale.TabIndex = 5;
            this.numScale.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numScale.ValueChanged += new System.EventHandler(this.numScale_ValueChanged);
            // 
            // lblScale
            // 
            this.lblScale.AutoSize = true;
            this.lblScale.Location = new System.Drawing.Point(5, 172);
            this.lblScale.Name = "lblScale";
            this.lblScale.Size = new System.Drawing.Size(108, 17);
            this.lblScale.TabIndex = 4;
            this.lblScale.Text = "Skaliranje (oba)";
            // 
            // numRotate
            // 
            this.numRotate.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numRotate.Location = new System.Drawing.Point(8, 105);
            this.numRotate.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numRotate.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numRotate.Name = "numRotate";
            this.numRotate.Size = new System.Drawing.Size(120, 22);
            this.numRotate.TabIndex = 3;
            this.numRotate.ValueChanged += new System.EventHandler(this.numRotate_ValueChanged);
            // 
            // lblRotate
            // 
            this.lblRotate.AutoSize = true;
            this.lblRotate.Location = new System.Drawing.Point(5, 85);
            this.lblRotate.Name = "lblRotate";
            this.lblRotate.Size = new System.Drawing.Size(141, 17);
            this.lblRotate.TabIndex = 2;
            this.lblRotate.Text = "Rotacija (desni bolid)";
            // 
            // numTranslate
            // 
            this.numTranslate.Location = new System.Drawing.Point(8, 29);
            this.numTranslate.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numTranslate.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numTranslate.Name = "numTranslate";
            this.numTranslate.Size = new System.Drawing.Size(120, 22);
            this.numTranslate.TabIndex = 1;
            this.numTranslate.ValueChanged += new System.EventHandler(this.numTranslate_ValueChanged);
            // 
            // lblTranslate
            // 
            this.lblTranslate.AutoSize = true;
            this.lblTranslate.Location = new System.Drawing.Point(5, 9);
            this.lblTranslate.Name = "lblTranslate";
            this.lblTranslate.Size = new System.Drawing.Size(146, 17);
            this.lblTranslate.TabIndex = 0;
            this.lblTranslate.Text = "Translacija (levi bolid)";
            // 
            // timer
            // 
            this.timer.Interval = 20;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // gbControls
            // 
            this.gbControls.Controls.Add(this.label10);
            this.gbControls.Controls.Add(this.label9);
            this.gbControls.Controls.Add(this.label8);
            this.gbControls.Controls.Add(this.label7);
            this.gbControls.Controls.Add(this.label6);
            this.gbControls.Controls.Add(this.label5);
            this.gbControls.Controls.Add(this.label4);
            this.gbControls.Controls.Add(this.label3);
            this.gbControls.Controls.Add(this.label2);
            this.gbControls.Controls.Add(this.label1);
            this.gbControls.Controls.Add(this.lblTut1);
            this.gbControls.Location = new System.Drawing.Point(5, 329);
            this.gbControls.Name = "gbControls";
            this.gbControls.Size = new System.Drawing.Size(155, 360);
            this.gbControls.TabIndex = 8;
            this.gbControls.TabStop = false;
            this.gbControls.Text = "Kontrole";
            // 
            // lblTut1
            // 
            this.lblTut1.AutoSize = true;
            this.lblTut1.Location = new System.Drawing.Point(6, 33);
            this.lblTut1.Name = "lblTut1";
            this.lblTut1.Size = new System.Drawing.Size(101, 17);
            this.lblTut1.TabIndex = 0;
            this.lblTut1.Text = "Rotacija scene";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "gore - I";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "dole - K";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "levo - J";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "desno - L";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Pokretanje";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "animacije - V";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 17);
            this.label7.TabIndex = 7;
            this.label7.Text = "Zumiranje";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 199);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 17);
            this.label8.TabIndex = 8;
            this.label8.Text = "napred - Num+";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 216);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 17);
            this.label9.TabIndex = 9;
            this.label9.Text = "nazad - Num-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 245);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 17);
            this.label10.TabIndex = 10;
            this.label10.Text = "Izlaz - F4";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 692);
            this.Controls.Add(this.panelParams);
            this.Controls.Add(this.openglControl);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Form";
            this.panelParams.ResumeLayout(false);
            this.panelParams.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRotate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTranslate)).EndInit();
            this.gbControls.ResumeLayout(false);
            this.gbControls.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public Tao.Platform.Windows.SimpleOpenGlControl openglControl;
        private System.Windows.Forms.Panel panelParams;
        private System.Windows.Forms.NumericUpDown numTranslate;
        private System.Windows.Forms.Label lblTranslate;
        private System.Windows.Forms.NumericUpDown numScale;
        private System.Windows.Forms.Label lblScale;
        private System.Windows.Forms.NumericUpDown numRotate;
        private System.Windows.Forms.Label lblRotate;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label lblAnimate;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.GroupBox gbControls;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTut1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;

    }
}

