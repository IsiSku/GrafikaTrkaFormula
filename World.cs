﻿// -----------------------------------------------------------------------
// <file>World.cs</file>
// <copyright>Grupa za Grafiku, Interakciju i Multimediju 2013.</copyright>
// <author>Zoran Milicevic</author>
// <summary>Klasa koja enkapsulira OpenGL programski kod.</summary>
// -----------------------------------------------------------------------
namespace RacunarskaGrafika.Vezbe.AssimpNetSample
{
    using System;
    using Tao.OpenGl;
    using Assimp;
    using System.IO;
    using System.Reflection;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Windows.Forms;

    /// <summary>
    ///  Klasa enkapsulira OpenGL kod i omogucava njegovo iscrtavanje i azuriranje.
    /// </summary>
    public class World : IDisposable
    {
        #region Atributi

        /// <summary>
        /// Identifikatori tekstura za jednostavniji pristup teksturama
        /// </summary>
        private enum TextureObjects { Gravel = 0, Asphalt, Fence };
        private readonly int m_textureCount = Enum.GetNames(typeof(TextureObjects)).Length;

        /// <summary>
        /// Identifikatori OpenGL tekstura
        /// </summary>
        private int[] m_textures = null;

        /// <summary>
        /// Putanje do slika koje se koriste za teksture
        /// </summary>
        private string[] m_textureFiles = { Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "img\\gravel.jpg"),
                                            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "img\\asphalt.jpg"),
                                            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "img\\fence.png") };

        /// <summary>
        ///	 Scena koja se prikazuje.
        /// </summary>
        private AssimpScene m_scene;

        /// <summary>
        ///	 Identifikator fonta.
        /// </summary>
        private BitmapFont m_font = null;

        /// <summary>
        ///	 Ugao rotacije sveta oko X ose.
        /// </summary>
        private float m_xRotation = 0.0f;

        /// <summary>
        ///	 Ugao rotacije sveta oko Y ose.
        /// </summary>
        private float m_yRotation = 0.0f;

        /// <summary>
        ///	 Udaljenost scene od kamere.
        /// </summary>
        private float m_sceneDistance = 20.0f;

        /// <summary>
        ///	 Sirina OpenGL kontrole u pikselima.
        /// </summary>
        private int m_width;

        /// <summary>
        ///	 Visina OpenGL kontrole u pikselima.
        /// </summary>
        private int m_height;

        /// <summary>
        /// Razdaljina levog bolida od pocetne pozicije po x-osi
        /// </summary>
        private int trans = 0;

        /// <summary>
        /// Ugao za koji je zarotiran desni bolid
        /// </summary>
        private int rot = 0;

        /// <summary>
        /// Pomeraj prvog bolida za vreme animacije
        /// </summary>
        private float deltaFront1 = 0;
        /// <summary>
        /// Pomeraj drugog bolida za vreme animacije
        /// </summary>
        private float deltaFront2 = 0;

        /// <summary>
        /// Udaljenost cilja
        /// </summary>
        private float finishDist = -100f;

        /// <summary>
        /// Velicina oba bolida
        /// </summary>
        private float scale = 1f;

        #endregion Atributi

        #region Properties

        /// <summary>
        ///	 Scena koja se prikazuje.
        /// </summary>
        public AssimpScene Scene
        {
            get { return m_scene; }
            set { m_scene = value; }
        }

        /// <summary>
        ///	 Ugao rotacije sveta oko X ose.
        /// </summary>
        public float RotationX
        {
            get { return m_xRotation; }
            set { m_xRotation = value; }
        }

        /// <summary>
        ///	 Ugao rotacije sveta oko Y ose.
        /// </summary>
        public float RotationY
        {
            get { return m_yRotation; }
            set { m_yRotation = value; }
        }

        /// <summary>
        ///	 Udaljenost scene od kamere.
        /// </summary>
        public float SceneDistance
        {
            get { return m_sceneDistance; }
            set { m_sceneDistance = value; }
        }

        /// <summary>
        ///	 Sirina OpenGL kontrole u pikselima.
        /// </summary>
        public int Width
        {
            get { return m_width; }
            set { m_width = value; }
        }

        /// <summary>
        ///	 Visina OpenGL kontrole u pikselima.
        /// </summary>
        public int Height
        {
            get { return m_height; }
            set { m_height = value; }
        }

        #endregion Properties

        #region Konstruktori

        /// <summary>
        ///  Konstruktor klase World.
        /// </summary>
        public World(String scenePath1, String sceneFileName1, String scenePath2, String sceneFileName2, int width, int height)
        {
            //Scena
            this.m_scene = new AssimpScene(scenePath1, sceneFileName1, scenePath2, sceneFileName2);
            this.m_width = width;
            this.m_height = height;

            //Font
            try
            {
                m_font = new BitmapFont("Arial", 14, false, false, true, false);
                m_textures = new int[m_textureCount];
                m_textures[(int)TextureObjects.Gravel] = 101;
                m_textures[(int)TextureObjects.Asphalt] = 102;
                m_textures[(int)TextureObjects.Fence] = 103;
            }
            catch (Exception)
            {
                MessageBox.Show("Neuspesno kreirana instanca OpenGL fonta", "GRESKA", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.Initialize();  // Korisnicka inicijalizacija OpenGL parametara

            this.Resize();      // Podesi projekciju i viewport
        }

        /// <summary>
        ///  Destruktor klase World.
        /// </summary>
        ~World()
        {
            this.Dispose(false);
        }

        #endregion Konstruktori

        #region Metode

        /// <summary>
        /// Translira levi bolid na zadatu razdaljinu po x-osi.
        /// </summary>
        /// <param name="delta">Razdaljna bolida od pocetne pozicije</param>
        public void Translate(int delta)
        {
            Console.WriteLine("Translating left car: " + delta);
            trans = delta;
        }

        /// <summary>
        /// Rotira desni bolid na zadati ugao u stepenima
        /// </summary>
        /// <param name="delta">Ugao bolida od pocetne pozicije</param>
        public void Rotate(int delta)
        {
            Console.WriteLine("Rotating right car: " + delta);
            rot = delta;
        }

        /// <summary>
        /// Skalira oba bolida na zadatu velicinu
        /// </summary>
        /// <param name="delta">Uvecanje bolida</param>
        public void Scale(float delta)
        {
            Console.WriteLine("Scaling both cars: " + delta);
            scale = delta;
        }

        /// <summary>
        ///  Iscrtavanje OpenGL kontrole.
        /// </summary>
        public void Draw()
        {
            Console.WriteLine("Drawing OpenGL");
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Resize();

            //Pogledaj
            Glu.gluLookAt(0.0f, 10.0f, -40.0f,
                          0.0f, 0.0f, -15.0f,
                          0.0f, 1.0f, 0.0f);

            //Namesti
            Gl.glPushMatrix();
                Gl.glTranslatef(0.0f, 0.0f, -m_sceneDistance);
                Gl.glRotatef(m_xRotation, 1.0f, 0.0f, 0.0f);
                Gl.glRotatef(m_yRotation, 0.0f, 1.0f, 0.0f);

                //PODLOGA - skaliranje matricom
                Gl.glPushMatrix();
                    Gl.glColor3f(0.2f, 0.1f, 0);
                    Gl.glBindTexture(Gl.GL_TEXTURE_2D, m_textures[(int)TextureObjects.Gravel]);
                    Gl.glMatrixMode(Gl.GL_TEXTURE);
                    Gl.glPushMatrix();
                        Gl.glScalef(10.0f, 100.0f, 1.0f);
                        Gl.glMatrixMode(Gl.GL_MODELVIEW);
            
                        Gl.glBegin(Gl.GL_QUADS);
                            //Normala
                            //Gl.glNormal3fv(Lighting.FindFaceNormal(-20.0f, -0.71f, 25.0f, 20.0f, -0.71f, 25.0f, 20.0f, -0.71f, -150.0f));
                            Gl.glNormal3f(0f, 1f, 0f);
                            //dole levo
                            Gl.glTexCoord2f(0.0f, 0.0f);
                            Gl.glVertex3f(-20.0f, -0.71f, 25.0f);
                            //dole desno
                            Gl.glTexCoord2f(1.0f, 0.0f);
                            Gl.glVertex3f(20.0f, -0.71f, 25.0f);
                            //gore desno
                            Gl.glTexCoord2f(1.0f, 1.0f);
                            Gl.glVertex3f(20.0f, -0.71f, -150.0f);
                            //gore levo
                            Gl.glTexCoord2f(0.0f, 1.0f);
                            Gl.glVertex3f(-20.0f, -0.71f, -150.0f);
                        Gl.glEnd();
                        Gl.glMatrixMode(Gl.GL_TEXTURE);
                    Gl.glPopMatrix();
                    Gl.glMatrixMode(Gl.GL_MODELVIEW);
                Gl.glPopMatrix();

                //STAZA - skaliranje dimenzijama
                Gl.glBindTexture(Gl.GL_TEXTURE_2D, m_textures[(int)TextureObjects.Asphalt]);
                Gl.glColor3f(0.0f, 0.0f, 0.15f);
                Gl.glBegin(Gl.GL_QUADS);
                    //Normala
                    //Gl.glNormal3fv(Lighting.FindFaceNormal(-10.0f, -0.7f, 20.0f, 10.0f, -0.7f, 20.0f, 10.0f, -0.7f, -100.0f));
                    Gl.glNormal3f(0f, 1f, 0f);
                    //dole levo
                    Gl.glTexCoord2f(0.0f, 0.0f);
                    Gl.glVertex3f(-10.0f, -0.7f, 20.0f);
                    //dole desno
                    Gl.glTexCoord2f(10.0f, 0.0f);
                    Gl.glVertex3f(10.0f, -0.7f, 20.0f);
                    //gore desno
                    Gl.glTexCoord2f(10.0f, 100.0f);
                    Gl.glVertex3f(10.0f, -0.7f, -100.0f);
                    //gore levo
                    Gl.glTexCoord2f(0.0f, 100.0f);
                    Gl.glVertex3f(-10.0f, -0.7f, -100.0f);
                Gl.glEnd();

                //BANKINE
                Box b = new Box(0.1, 5, 120, m_textures[(int)TextureObjects.Fence]);
                //Bele - osencene (zuto)
                Gl.glColor3f(0.0f, 0.0f, 0.0f);
                Gl.glPushMatrix();
                    Gl.glTranslatef(-10.0f, 2.0f, -40.0f);
                    b.Draw();
                    Gl.glTranslatef(20.0f, 0.0f, 0.0f);
                    b.Draw();
                Gl.glPopMatrix();

                //Gasimo zbog elemenata koji se boje rucno
                Gl.glDisable(Gl.GL_TEXTURE_2D);
                //CILJ
                Box c = new Box(1, 5, 1, 0);
                Gl.glColor3f(1.0f, 1.0f, 1.0f);
                Gl.glPushMatrix();
                    Gl.glTranslatef(-9.0f, 2.0f, finishDist);
                    c.Draw();
                    Gl.glTranslatef(18.0f, 0.0f, 0.0f);
                    c.Draw();
                Gl.glPopMatrix();

                //KOLA
                Gl.glEnable(Gl.GL_TEXTURE_2D);
                float[] lightPosCars = { 0f, 15f, 0f, 1f };
                Gl.glLightfv(Gl.GL_LIGHT1, Gl.GL_POSITION, lightPosCars);
                m_scene.Draw(-trans, rot, scale);


            //Vrati
            Gl.glPopMatrix();

            //Deo sa tekstom
            String msg1 = "Predmet: Racunarska grafika";
            String msg2 = "Sk.god: 2015/16.";
            String msg3 = "Ime: Isidora";
            String msg4 = "Prezime: Skulec";
            String msg5 = "Sifra zad: 2.1";

            int dimX = (int)Math.Round(m_font.CalculateTextWidth(msg1));
            int dimY = (int)Math.Round(m_font.CalculateTextHeight(msg1) * 5);
            int th = 50;

            Gl.glDisable(Gl.GL_LIGHTING);
            Gl.glColor3f(0.0f, 1.0f, 1.0f);
            Gl.glViewport(m_width-dimX, 0, dimX, dimY);
            Gl.glMatrixMode(Gl.GL_PROJECTION);
                Gl.glLoadIdentity();
                Glu.gluOrtho2D(-dimX, dimX, -dimY, dimY);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
                Gl.glLoadIdentity();

            Gl.glTranslatef(0.0f, 0.0f, 1.0f);
            Gl.glRasterPos2d(-dimX, 2 * th);
            m_font.DrawText(msg1);
            Gl.glRasterPos2d(-dimX, 1 * th);
            m_font.DrawText(msg2);
            Gl.glRasterPos2d(-dimX, 0 * th);
            m_font.DrawText(msg3);
            Gl.glRasterPos2d(-dimX, -1 * th);
            m_font.DrawText(msg4);
            Gl.glRasterPos2d(-dimX, -2 * th);
            m_font.DrawText(msg5);

            // Oznaci kraj iscrtavanja
            Gl.glFlush();
        }

        /// <summary>
        /// Vraca pomeraje bolida na 0
        /// </summary>
        public void ResetAnim()
        {
            deltaFront1 = 0;
            deltaFront2 = 0;

            RotationX = 0;
            RotationY = 0;
            SceneDistance = 20;
        }

        /// <summary>
        /// Uvecava pomeraje bolida da izgleda kao da se trkaju
        /// </summary>
        public void Update(MainForm mf)
        {
            deltaFront1 -= 0.4f;
            deltaFront2 -= 0.3f;
            m_scene.Update(deltaFront1, deltaFront2);

            if (deltaFront1 < finishDist)
            {
                mf.stopAnimation();
            }
        }

        /// <summary>
        ///  Korisnicka inicijalizacija i podesavanje OpenGL parametara.
        /// </summary>
        private void Initialize()
        {
            // Vrednosti svetlosnih komponenti
            float[] ambientLight = { 0.1f, 0.1f, 0.1f, 1.0f };
            float[] diffuseLightScene = { 1f, 1f, 0.3f, 1.0f };
            float[] diffuseLightCars = { 1.0f, 1.0f, 1.0f, 1.0f };

            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glEnable(Gl.GL_CULL_FACE);

            // Predji u rezim rada sa 2D teksturama
            Gl.glEnable(Gl.GL_TEXTURE_2D);
            // Podesi nacin blending teksture
            Gl.glTexEnvi(Gl.GL_TEXTURE_ENV, Gl.GL_TEXTURE_ENV_MODE, Gl.GL_DECAL);

            // Ucitaj slike i kreiraj teksture
            Gl.glGenTextures(m_textureCount, m_textures);
            for (int i = 0; i < m_textureCount; ++i)
            {
                // Pridruzi teksturu odgovarajucem identifikatoru
                Gl.glBindTexture(Gl.GL_TEXTURE_2D, m_textures[i]);
                // Ucitaj sliku i podesi parametre teksture
                Bitmap image = new Bitmap(m_textureFiles[i]);
                // rotiramo sliku zbog koordinantog sistema opengl-a
                image.RotateFlip(RotateFlipType.RotateNoneFlipY);
                Rectangle rect = new Rectangle(0, 0, image.Width, image.Height);
                // RGBA format (dozvoljena providnost slike tj. alfa kanal)
                BitmapData imageData = image.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                // Kreiraj teksturu sa RGBA
                Gl.glTexImage2D(Gl.GL_TEXTURE_2D, 0, (int)Gl.GL_RGBA8, imageData.Width, imageData.Height, 0, Gl.GL_BGRA, Gl.GL_UNSIGNED_BYTE, imageData.Scan0);
                // Posto je kreirana tekstura slika nam vise ne treba
                image.UnlockBits(imageData);
                image.Dispose();
            }

            
            // Podesi parametre teksture
            foreach (int textureId in m_textures)
            {
                Gl.glBindTexture(Gl.GL_TEXTURE_2D, textureId);
                Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MIN_FILTER, Gl.GL_LINEAR);
                Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MAG_FILTER, Gl.GL_LINEAR);
                Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_WRAP_S, Gl.GL_REPEAT);
                Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_WRAP_T, Gl.GL_REPEAT);
            }
            /**/
            
            // Ukljuci proracun osvetljenja
            Gl.glEnable(Gl.GL_LIGHTING);

            // Specifikuj i ukljuci svetlosni izvor 0
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_AMBIENT, ambientLight);
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_DIFFUSE, diffuseLightScene);
            // Podesi parametre tackastog svetlosnog izvora
            Gl.glLightf(Gl.GL_LIGHT0, Gl.GL_SPOT_CUTOFF, 180.0f);
            // Ukljuci svetlosni izvor
            Gl.glEnable(Gl.GL_LIGHT0);

            // Specifikuj i ukljuci svetlosni izvor 1
            Gl.glLightfv(Gl.GL_LIGHT1, Gl.GL_AMBIENT, ambientLight);
            Gl.glLightfv(Gl.GL_LIGHT1, Gl.GL_DIFFUSE, diffuseLightCars);
            // Podesi parametre tackastog svetlosnog izvora
            Gl.glLightf(Gl.GL_LIGHT1, Gl.GL_SPOT_CUTOFF, 45.0f);
            // Ukljuci svetlosni izvor
            Gl.glEnable(Gl.GL_LIGHT1);

            // Ukljuci color tracking
            Gl.glEnable(Gl.GL_COLOR_MATERIAL);
            // Podesi na koje parametre materijala se odnose pozivi glColor funkcije
            Gl.glColorMaterial(Gl.GL_FRONT, Gl.GL_AMBIENT_AND_DIFFUSE);

            // Boja pozadine je bela
            Gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

            // Ukljuci automatsku normalizaciju nad normalama
            Gl.glEnable(Gl.GL_NORMALIZE);

            
        }

        /// <summary>
        /// Podesava viewport i projekciju za OpenGL kontrolu.
        /// </summary>
        public void Resize()
        {
            float[] lightPosScene = { -2f, 2f, -2f, 1.0f };
            

            Gl.glViewport(0, 0, m_width, m_height); // kreiraj viewport po celom prozoru
            Gl.glMatrixMode(Gl.GL_PROJECTION);      // selektuj Projection Matrix
            Gl.glLoadIdentity();			        // resetuj Projection Matrix

            Glu.gluPerspective(50.0, (double)m_width / (double)m_height, 1.0, 20000.0);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);   // selektuj ModelView Matrix
            Gl.glLoadIdentity();                // resetuj ModelView Matrix

            // Pozicioniranje svetlosnog izvora, koji je neosetljiv na transformacije
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, lightPosScene);

            Gl.glEnable(Gl.GL_TEXTURE_2D);
            Gl.glTexEnvi(Gl.GL_TEXTURE_ENV, Gl.GL_TEXTURE_ENV_MODE, Gl.GL_DECAL);
        }

        /// <summary>
        ///  Implementacija IDisposable interfejsa.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Oslodi managed resurse
            }

            // Oslobodi unmanaged resurse
            Terminate();
            m_font.Dispose();
            m_scene.Dispose();
        }
        private void Terminate()
        {
            Gl.glDeleteTextures(m_textureCount, m_textures);
        }

        #endregion Metode

        #region IDisposable metode

        /// <summary>
        ///  Dispose metoda.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable metode
    }
}
