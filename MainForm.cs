﻿// -----------------------------------------------------------------------
// <file>MainForm.cs</file>
// <copyright>Grupa za Grafiku, Interakciju i Multimediju 2013.</copyright>
// <author>Zoran Milicevic</author>
// <summary>Demonstracija ucitavanja modela pomocu AssimpNet biblioteke i koriscenja u OpenGL-u.</summary>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Assimp;
using System.IO;
using System.Reflection;

namespace RacunarskaGrafika.Vezbe.AssimpNetSample
{
    public partial class MainForm : Form
    {
        #region Atributi

        /// <summary>
        ///	 Instanca OpenGL "sveta" - klase koja je zaduzena za iscrtavanje koriscenjem OpenGL-a.
        /// </summary>
        World m_world = null;

        #endregion Atributi

        #region Konstruktori

        public MainForm()
        {
            // Inicijalizacija komponenti
            InitializeComponent();

            // Inicijalizacija OpenGL konteksta
            openglControl.InitializeContexts();

            // Kreiranje OpenGL sveta
            try
            {               
                //Verzija sa teksturama
                String path1 = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "3D Models\\Ferrari");
                String path2 = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "3D Models\\Mercedes");

                m_world = new World(path1, "Car.lwo", path2, "Car.lwo", openglControl.Width, openglControl.Height);
            }
            catch (Exception e)
            {
                MessageBox.Show("Neuspesno kreirana instanca OpenGL sveta. Poruka greške: " + e.Message, "GRESKA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        #endregion Konstruktori

        #region Rukovaoci dogadjajima OpenGL kontrole

        /// <summary>
        /// Rukovalac dogadja izmene dimenzija OpenGL kontrole
        /// </summary>
        private void OpenGlControlResize(object sender, EventArgs e)
        {
            m_world.Height = openglControl.Height;
            m_world.Width = openglControl.Width;

            m_world.Resize();
        }

        /// <summary>
        /// Rukovalac dogadjaja iscrtavanja OpenGL kontrole
        /// </summary>
        private void OpenGlControlPaint(object sender, PaintEventArgs e)
        {
            // Iscrtaj svet
            m_world.Draw();
        }

        public void stopAnimation()
        {
            timer.Stop();

            m_world.Translate(0);
            m_world.Rotate(0);
            m_world.Scale(1);

            numTranslate.Visible = true;
            numRotate.Visible = true;
            numScale.Visible = true;

            numTranslate.Value = 0;
            numRotate.Value = 0;
            numScale.Value = 1;
            m_world.ResetAnim();
            m_world.Update(this);

            lblRotate.Visible = true;
            lblTranslate.Visible = true;
            lblScale.Visible = true;
            lblAnimate.Visible = false;

            gbControls.Visible = true;

            btnReset.Visible = true;
        }

        public void startAnimation()
        {
            m_world.Translate(0);
            m_world.Rotate(0);
            m_world.Scale(1);

            numTranslate.Value = 0;
            numRotate.Value = 0;
            numScale.Value = 1;
            m_world.ResetAnim();
            m_world.RotationX = -50;

            numTranslate.Visible = false;
            numRotate.Visible = false;
            numScale.Visible = false;

            lblRotate.Visible = false;
            lblTranslate.Visible = false;
            lblScale.Visible = false;
            lblAnimate.Visible = true;

            gbControls.Visible = false;

            btnReset.Visible = false;

            timer.Start();
        }

        /// <summary>
        /// Rukovalac dogadjaja: obrada tastera nad formom
        /// </summary>
        private void OpenGlControlKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F4:
                    if (!timer.Enabled)
                    {
                        this.Close();
                    }
                    break;
                case Keys.I:
                    if (m_world.RotationX - 5.0f >= -65 && !timer.Enabled)
                    {
                        m_world.RotationX -= 5.0f;
                    }
                    break;
                case Keys.K:
                    if (m_world.RotationX + 5.0f <= 0 && !timer.Enabled)
                    {
                        m_world.RotationX += 5.0f;
                    }
                    break;
                case Keys.J:
                    if (!timer.Enabled)
                    {
                        m_world.RotationY -= 5.0f;
                    }
                    break;
                case Keys.L:
                    if (!timer.Enabled)
                    {
                        m_world.RotationY += 5.0f;
                    }
                    break;
                case Keys.Add:
                    if (!timer.Enabled)
                    {
                        m_world.SceneDistance += 5.0f;
                        m_world.Resize();
                    }
                    break;
                case Keys.Subtract:
                    if (!timer.Enabled)
                    {
                        m_world.SceneDistance -= 5.0f;
                        m_world.Resize();
                    }
                    break;
                case Keys.V:
                    if (!timer.Enabled)
                    {
                        startAnimation();
                    }
                    break;
            }

            openglControl.Refresh();
        }

        #endregion Rukovaoci dogadjajima OpenGL kontrole

        #region Rukovaoci dogadjajima WFA kontrola

        private void numTranslate_ValueChanged(object sender, EventArgs e)
        {
            m_world.Translate((int)((NumericUpDown)sender).Value);
            openglControl.Refresh();
        }
        

        private void numRotate_ValueChanged(object sender, EventArgs e)
        {
            m_world.Rotate((int)((NumericUpDown)sender).Value);
            openglControl.Refresh();
        }

        private void numScale_ValueChanged(object sender, EventArgs e)
        {
            m_world.Scale((float)((NumericUpDown)sender).Value);
            openglControl.Refresh();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            // Azuriraj svet i ponovno ga iscrtaj
            m_world.Update(this);
            m_world.SceneDistance -= 0.6f;
            m_world.Resize();
            openglControl.Refresh();
        }

        #endregion

        private void btnReset_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Click");
            m_world.Translate(0);
            m_world.Rotate(0);
            m_world.Scale(1);

            numTranslate.Value = 0;
            numRotate.Value = 0;
            numScale.Value = 1;

            m_world.Update(this);
            m_world.ResetAnim();
            openglControl.Refresh();
        }


    }
}
